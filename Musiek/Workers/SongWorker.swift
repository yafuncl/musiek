//
//  NetworkWorker.swift
//  Musiek
//
//  Created by Diego Vargas on 20-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

class SongWorker {
    
    var networker: NetworkerProtocol = Networker()

     func fetchSongs(completionHandler: @escaping ([Decodable]?, Error?) -> Void) {
        networker.getSongs(successCompletion: { result in
            print(result)
        }) { error in
            print(error)
        }
    }

}
