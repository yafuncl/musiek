//
//  Networker.swift
//  Musiek
//
//  Created by Diego Vargas on 20-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
import Alamofire


protocol NetworkerProtocol {
    func getSongs(successCompletion: @escaping ([Song]?) -> Void, failureCompletion: @escaping (Error?) -> Void)
}

class Networker: NetworkerProtocol {

    // Dependency Injection for testing
    let session: URLSession
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }

    func getSongs(successCompletion: @escaping ([Song]?) -> Void, failureCompletion: @escaping (Error?) -> Void) {

        var songListArray: [Song] = []

        let url = "https://www.apple.com/mac/"

        guard let urlStable = URL(string: url) else { return }
        let request = URLRequest(url: urlStable)

        let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard error == nil else {
                failureCompletion(error)
                return }
            guard let data = data else { return }

            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject] {
                    for dict in json {
                        guard let artistName = dict["artistName"] as? String else { return }
                        guard let trackName = dict["trackName"] as? String else { return }
                        guard let trackId = dict["trackId"] as? Int else { return }
                        guard let trackNumber = dict["trackNumber"] as? Int else { return }
                        guard let collectionId = dict["collectionId"] as? Int else { return }
                        guard let collectionName = dict["collectionName"] as? String else { return }
                        guard let artworkUrl100 = dict["artworkUrl100"] as? String else { return }
                        let song = Song(artistName: artistName, trackName: trackName, trackId: trackId, trackNumber: trackNumber, collectionId: collectionId, collectionName: collectionName, artworkUrl100: artworkUrl100)
                        songListArray.append(song)
                    }
                    DispatchQueue.main.async {
                        successCompletion(songListArray)
                    }
                }
            } catch {
                DispatchQueue.main.async {

                    // TODO: - Remove when backend is ready. For now use fake data.
                    songListArray = [
                        Song(artistName: "nombre", trackName: "track", trackId: 1, trackNumber: 2, collectionId: 3, collectionName: "album name", artworkUrl100: "artwork"),
                        Song(artistName: "nombre", trackName: "track", trackId: 2, trackNumber: 3, collectionId: 3, collectionName: "album name", artworkUrl100: "artwork")
                    ]

                    successCompletion(songListArray)
                    //                    failureCompletion(error)
                }
            }
        }
        task.resume()
    }
}
