//
//  DetailView.swift
//  Musiek
//
//  Created by Diego Vargas on 15-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import SwiftUI
import KingfisherSwiftUI
import Alamofire

struct DetailView: View {
    @State var songItem: Song
    @State var songList: [Song]
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var body: some View {
        VStack{
            KFImage(URL(string: self.songItem.artworkUrl100))
                .frame(width: 300, height: 100)
                .shadow(radius: 5)
            Text(self.songItem.collectionName).font(.title).multilineTextAlignment(.center)
            Text(self.songItem.artistName).multilineTextAlignment(.center)
            Divider()
            Text("Canciones").multilineTextAlignment(.leading).font(.caption)
            List {
                Section(header: Text("Escucha una vista previa")) {
                    ForEach(self.songList, id: \.trackName) { item in
                        Text(item.trackName)
                        //return SongRow(songItem: item, songList: [])
                    }
                }
            }.listStyle(GroupedListStyle()).environment(\.horizontalSizeClass, .regular).onAppear {
                print("HAHA")
                AF.request("https://itunes.apple.com/lookup?id=\(self.songItem.collectionId)&entity=song").validate().responseDecodable(of: Songs.self) { response in
                    guard let songs = response.value else { return }
                    self.songList = songs.all
                    print(songs.all)
                }
            }
        }
    }
}
