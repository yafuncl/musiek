//
//  ContentView.swift
//  Musiek
//
//  Created by Diego Vargas on 14-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import SwiftUI
import Alamofire

struct MainView: View {
    @State var searchString: String = ""
    @State var songList: [Song] = {
        guard let data = UserDefaults.standard.data(forKey: "songs") else { return [] }
               if let json = try? JSONDecoder().decode([Song].self, from: data) {
                   return json
               }
               return []
    }()
    var body: some View {
        NavigationView {
            VStack {
                HStack(spacing: 5) {
                    Text("🔎").padding(.trailing)
                    Group {
                        TextField("Ingrese una canción para buscar", text: $searchString) {
                            AF.request("https://itunes.apple.com/search?term=\(self.$searchString.wrappedValue)&mediaType=album&limit=20").validate().responseDecodable(of: Songs.self) { response in
                                guard let songs = response.value else { return }
                                self.songList = songs.all
                            }
                        }.padding(.all, 12)
                    }.background(Color.yellow)
                        .clipShape(RoundedRectangle(cornerRadius: 5))
                        .shadow(radius: 2)
                        .padding(.trailing, 8)
                }.padding(20)
                Text("🎵🎶🎸🎶🎸🎷🎵🎧")
                    .bold()
                    .font(.largeTitle)
                    .foregroundColor(Color.red)
                Divider()
                List {
                    ForEach(self.songList, id: \.trackName) { item in
                        return SongRow(songItem: item, songList: self.songList)
                    }
                }
            }
            .navigationBarTitle(Text("Musiek"))
        }
    }
    
    public func fetch(searchString: String) {
        AF.request("https://itunes.apple.com/search?term=\(searchString)&mediaType=album&limit=20").validate().responseDecodable(of: Songs.self) { response in
        guard let songs = response.value else { return }
        self.songList = songs.all
        }
    }
}
