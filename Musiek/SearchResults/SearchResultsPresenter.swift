//
//  SearchResultsPresenter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol SearchResultsPresenterProtocol {
    func getResults(successCompletion: @escaping ([SearchResultsModel]?) -> Void,failureCompletion: @escaping (String) -> Void)
    func downloadImage(urlString: String,completion: @escaping (Data) -> Void)
    func showErrorAlert(error: String)
    func handleDidSelectRow(indexPathRow: Int)
    var searchResultsArray: [SearchResultsModel] { get set }
    var searchEntered: String? { get set }
}

class SearchResultsPresenter: SearchResultsPresenterProtocol {
    
    var searchResultsArray = [SearchResultsModel]()
    var searchEntered: String?
    weak var view: SearchResultsViewControllerProtocol?
    var router: SearchResultsRouterProtocol
    var api: SearchResultsAPIProtocol
    
    init(view: SearchResultsViewControllerProtocol, router: SearchResultsRouterProtocol, api: SearchResultsAPIProtocol) {
        self.view = view
        self.router = router
        self.api = api
    }
    func getResults(successCompletion: @escaping ([SearchResultsModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        api.getSearchResults(searchString: searchEntered , successCompletion: {[weak self] (receiveSearchResults) in
            if let receiveSearchResults = receiveSearchResults {
                for searchResult in receiveSearchResults {
                    self?.searchResultsArray.append(searchResult)
                }
                successCompletion(self?.searchResultsArray)
            }
        }) {(error) in
            failureCompletion(error)
        }
    }
    
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void) {
        api.downloadImage(urlString: urlString) { (data) in
            completion(data)
        }
    }
    
    func showErrorAlert(error: String) {
        router.showErrorAlert(error: error)
    }
    
    func handleDidSelectRow(indexPathRow: Int) {
        let selectedSearchResult = searchResultsArray[indexPathRow]
        if let searchEntered = searchEntered {
            router.showResultSelect(searchEntered: searchEntered, selectedSearchResult: selectedSearchResult)
        }
    }
}
