//
//  SearchResultsViewController.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

protocol SearchResultsViewControllerProtocol: class {
    func setUpUI()
}

class SearchResultsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SearchResultsViewControllerProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var presenter: SearchResultsPresenterProtocol = {
        let router = SearchResultsRouter(view: self) as SearchResultsRouterProtocol
        let api = Networker() as SearchResultsAPIProtocol
        return SearchResultsPresenter(view: self, router: router, api: api)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    func setUpUI() {
        if let searchEntered = presenter.searchEntered {
            let cleanTitle = searchEntered.replacingOccurrences(of: "+", with: " ")
            self.title = "Searching \(cleanTitle)"
        }
        let spinner = self.showModalSpinner()
        presenter.getResults(successCompletion: { [weak self] (_) in
            self?.hideModalSpinner(indicator: spinner)
            self?.tableView.reloadData()
        }) { [weak self] (error) in
            self?.presenter.showErrorAlert(error: error)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.searchResultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultsTableViewCell",  for: indexPath) as! SearchResultsTableViewCell
        let searchResult = presenter.searchResultsArray[indexPath.row]
        cell.searchResultNameLabel.text = searchResult.collectionName
        let localUrlString = searchResult.artworkUrl100
        presenter.downloadImage(urlString: localUrlString) { (data) in
            cell.searchResultImageView.image = UIImage(data: data)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handleDidSelectRow(indexPathRow: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("would get more results if api had pagination")
    }
}
