//
//  SearchResultsRouter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

protocol SearchResultsRouterProtocol {
    func showResultSelect(searchEntered: String, selectedSearchResult: SearchResultsModel)
    func showErrorAlert(error: String)
}

class SearchResultsRouter: SearchResultsRouterProtocol {
    
    weak var view: SearchResultsViewController?
    init(view: SearchResultsViewController) {
        self.view = view
    }
    
    func showResultSelect(searchEntered: String, selectedSearchResult: SearchResultsModel) {
        let storyboard = UIStoryboard(name: "MainStoryboard", bundle: nil)
        let albumViewController = storyboard.instantiateViewController(withIdentifier: "AlbumViewController") as! AlbumViewController
        albumViewController.presenter.selectedSearchResult = selectedSearchResult
        view?.navigationController?.pushViewController(albumViewController, animated: true)
    }
    
    func showErrorAlert(error: String) {
        if let view = view {
            Alerts.dismissableAlert(title: "Error".localized(), message: error, vc: view, actionBtnText: "Cancel".localized())
        }
    }
}
