//
//  SearchResultsAPI.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol SearchResultsAPIProtocol {
    func getSearchResults(searchString: String?, successCompletion: @escaping ([SearchResultsModel]?) -> Void,
                           failureCompletion: @escaping (String) -> Void)
    func downloadImage(urlString: String,
                       completion: @escaping (Data) -> Void)
}

extension Networker: SearchResultsAPIProtocol {
    func getSearchResults(searchString: String?, successCompletion: @escaping ([SearchResultsModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        let url = "https://itunes.apple.com/search?term=\(searchString ?? "any")&mediaType=album&limit=20"
        var searchResultsArray: [SearchResultsModel] = []
        guard let urlStable = URL(string: url) else { return }
        let request = URLRequest(url: urlStable)
        let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard error == nil else {
                failureCompletion(error!.localizedDescription)
                return }
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                failureCompletion("statusCode mishap: \(String(describing: (response as? HTTPURLResponse)?.statusCode))")
                return }
            guard let data = data else {
                failureCompletion("Error with data".localized())
                return }
            let contents = String(data: data, encoding: .utf8)
            var dictionary: [String:AnyObject]?
            if let stringData = contents?.data(using: .utf8) {
                do {
                    dictionary =  try JSONSerialization.jsonObject(with: stringData, options: [.allowFragments]) as? [String : AnyObject]
                    if let myDictionary = dictionary!["results"]
                    {
                        print(myDictionary)
                        let results = myDictionary as? [[String:AnyObject]]
                        for dict in results! {
                            
                            if dict.keys.contains("collectionId") && dict.keys.contains("trackName") && dict.keys.contains("trackNumber") {
                                guard let artistName = dict["artistName"] as? String else { return }
                                guard let trackName = dict["trackName"] as? String else { return }
                                guard let trackId = dict["trackId"] as? Int else { return }
                                guard let trackNumber = dict["trackNumber"] as? Int else { return }
                                guard let collectionId = dict["collectionId"] as? Int else { return }
                                guard let collectionName = dict["collectionName"] as? String else { return }
                                guard let artworkUrl100 = dict["artworkUrl100"] as? String else { return }
                                let searchResult = SearchResultsModel(artistName: artistName, trackName: trackName, trackId: trackId, trackNumber: trackNumber, collectionId: "\(collectionId)", collectionName: collectionName, artworkUrl100: artworkUrl100)
                                searchResultsArray.append(searchResult)
                            }
                            
                        }
                        DispatchQueue.main.async {
                            successCompletion(searchResultsArray)
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        failureCompletion(error.localizedDescription)
                    }
                }
            }
        }
        task.resume()
    }
}
