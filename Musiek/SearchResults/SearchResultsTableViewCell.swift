//
//  SearchResultsTableViewCell.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

class SearchResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var searchResultNameLabel: UILabel!
    @IBOutlet weak var searchResultImageView: UIImageView!
}
