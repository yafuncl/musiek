//
//  AlbumViewController.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

protocol AlbumViewControllerProtocol: class {
    func setUpUI()
}

class AlbumViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AlbumViewControllerProtocol  {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    lazy var presenter: AlbumPresenterProtocol = {
        let router = AlbumRouter(view: self) as AlbumRouterProtocol
        let api = Networker() as AlbumAPIProtocol
        return AlbumPresenter(view: self, router: router, api: api)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    func setUpUI() {
        if let selectedResult = presenter.selectedSearchResult {
            self.title = selectedResult.collectionName
            self.collectionNameLabel.text = selectedResult.collectionName
            self.artistNameLabel.text = selectedResult.artistName
            let localUrlString = selectedResult.artworkUrl100
            presenter.downloadImage(urlString: localUrlString) { (data) in
                self.imageView.image = UIImage(data: data)
            }
        }
        let spinner = self.showModalSpinner()
        presenter.getAlbumTracks(successCompletion: { [weak self] (_) in
            self?.hideModalSpinner(indicator: spinner)
            self?.tableView.reloadData()
        }) { [weak self] (error) in
            self?.presenter.showErrorAlert(error: error)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.albumTracksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTrackCell", for: indexPath) as! AlbumTableViewCell
        let albumTrack = presenter.albumTracksArray[indexPath.row]
        cell.albumTrack.text = albumTrack.trackName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handleDidSelectRow(indexPathRow: indexPath.row)
    }

}
