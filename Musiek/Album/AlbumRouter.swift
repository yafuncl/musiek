//
//  AlbumRouter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

protocol AlbumRouterProtocol {
    func showTrackSelect(selectedTrack: AlbumModel)
    func showErrorAlert(error: String)
}

class AlbumRouter: AlbumRouterProtocol {
    
    weak var view: AlbumViewController?
    
    init(view: AlbumViewController) {
        self.view = view
    }
    
    func showTrackSelect(selectedTrack: AlbumModel) {
        let storyboard = UIStoryboard(name: "MainStoryboard", bundle: nil)
        let songPreviewViewController = storyboard.instantiateViewController(withIdentifier: "SongPreviewViewController") as! SongPreviewViewController
        songPreviewViewController.presenter.selectedAlbumTrack = selectedTrack
        view?.navigationController?.pushViewController(songPreviewViewController, animated: true)
    }
    
    func showErrorAlert(error: String) {
        if let view = view {
            Alerts.dismissableAlert(title: "Error".localized(), message: error, vc: view, actionBtnText: "Cancel".localized())
        }
    }
}
