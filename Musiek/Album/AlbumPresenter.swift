//
//  AlbumPresenter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol AlbumPresenterProtocol {
    func getAlbumTracks(successCompletion: @escaping ([AlbumModel]?) -> Void, failureCompletion: @escaping (String) -> Void)
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void)
    func showErrorAlert(error: String)
    func handleDidSelectRow(indexPathRow: Int)
    var albumTracksArray: [AlbumModel] { get set }
    var selectedSearchResult: SearchResultsModel? { get set }
}

class AlbumPresenter: AlbumPresenterProtocol {
    
    var selectedSearchResult: SearchResultsModel?
    var albumTracksArray = [AlbumModel]()
    
    weak var view: AlbumViewControllerProtocol?
    var router: AlbumRouterProtocol
    var api: AlbumAPIProtocol
    
    init(view: AlbumViewControllerProtocol, router: AlbumRouterProtocol, api: AlbumAPIProtocol) {
        self.view = view
        self.router = router
        self.api = api
    }
    
    func getAlbumTracks(successCompletion: @escaping ([AlbumModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        
        if let selectedSearchResult = selectedSearchResult {
            api.getAlbumTracks(albumId: selectedSearchResult.collectionId, successCompletion: { [weak self] (receivedAlbumTracks) in
                if let receivedAlbumTracks = receivedAlbumTracks {
                    for track in receivedAlbumTracks {
                        self?.albumTracksArray.append(track)
                    }
                    successCompletion(self?.albumTracksArray)
                }
            }) { (error) in
                failureCompletion(error)
            }
        }
    }
    
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void) {
        api.downloadImage(urlString: urlString) { (data) in
            completion(data)
        }
    }
    
    func showErrorAlert(error: String) {
        router.showErrorAlert(error: error)
    }
    
    func handleDidSelectRow(indexPathRow: Int) {
        let selectedAlbumTrack = albumTracksArray[indexPathRow]
        router.showTrackSelect(selectedTrack: selectedAlbumTrack)
    }
}
