//
//  AlbumAPI.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol AlbumAPIProtocol {
    func getAlbumTracks(albumId: String, successCompletion: @escaping ([AlbumModel]?) -> Void, failureCompletion: @escaping (String) -> Void)
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void)
}

extension Networker: AlbumAPIProtocol {
    func getAlbumTracks(albumId: String, successCompletion: @escaping ([AlbumModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        let url = "https://itunes.apple.com/lookup?id=\(albumId)&entity=song"
        var searchResultsArray: [AlbumModel] = []
        
        guard let urlStable = URL(string: url) else { return }
        let request = URLRequest(url: urlStable)
        let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard error == nil else {
                failureCompletion(error!.localizedDescription)
                return }
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                failureCompletion("statusCode mishap: \(String(describing: (response as? HTTPURLResponse)?.statusCode))")
                return }
            guard let data = data else {
                failureCompletion("Error with data".localized())
                return }
            let contents = String(data: data, encoding: .utf8)
            var dictionary: [String:AnyObject]?
            if let stringData = contents?.data(using: .utf8) {
                do {
                    dictionary =  try JSONSerialization.jsonObject(with: stringData, options: [.allowFragments]) as? [String : AnyObject]
                    if let myDictionary = dictionary!["results"]
                    {
                        let results = myDictionary as? [[String:AnyObject]]
                        for dict in results! {
                            guard let wrapperType = dict["wrapperType"] as? String else { return }
                            if wrapperType == "track" {
                                guard let artistName = dict["artistName"] as? String else { return }
                                guard let trackName = dict["trackName"] as? String else { return }
                                guard let trackId = dict["trackId"] as? Int else { return }
                                guard let trackNumber = dict["trackNumber"] as? Int else { return }
                                guard let collectionId = dict["collectionId"] as? Int else { return }
                                guard let collectionName = dict["collectionName"] as? String else { return }
                                guard let previewUrl = dict["previewUrl"] as? String else { return }
                                guard let artworkUrl100 = dict["artworkUrl100"] as? String else { return }
                                let searchResult = AlbumModel(artistName: artistName, trackName: trackName, trackId: trackId, trackNumber: trackNumber, collectionId: collectionId, collectionName: collectionName, previewUrl: previewUrl, artworkUrl100: artworkUrl100)
                                searchResultsArray.append(searchResult)
                            }
                        }
                        DispatchQueue.main.async {
                            successCompletion(searchResultsArray)
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        failureCompletion(error.localizedDescription)
                    }
                }
            }
        }
        task.resume()
    }
}
