//
//  AlbumModel.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

struct AlbumModel: Equatable {
    let artistName: String
    let trackName: String
    let trackId: Int
    let trackNumber: Int
    let collectionId: Int
    let collectionName: String
    let previewUrl: String
    let artworkUrl100: String
}
