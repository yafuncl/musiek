//
//  SongPreviewAPI.swift
//  Musiek
//
//  Created by Diego Vargas on 24-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol SongPreviewAPIProtocol {
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void)
}

extension Networker: SongPreviewAPIProtocol {
    
}
