//
//  SongPreviewPresenter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol SongPreviewPresenterProtocol {
    func showErrorAlert(error: String)
    var selectedAlbumTrack: AlbumModel? { get set }
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void)
}

class SongPreviewPresenter: SongPreviewPresenterProtocol {
    
    var selectedAlbumTrack: AlbumModel?
    
    weak var view: SongPreviewViewControllerProtocol?
    var router: SongPreviewRouterProtocol
    var api: SongPreviewAPIProtocol
    
    init(view: SongPreviewViewControllerProtocol, router: SongPreviewRouterProtocol, api: SongPreviewAPIProtocol) {
        self.view = view
        self.router = router
        self.api = api
    }
    
    func showErrorAlert(error: String) {
        router.showErrorAlert(error: error)
    }
    
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void) {
        api.downloadImage(urlString: urlString) { (data) in
            completion(data)
        }
    }
    
    
}
