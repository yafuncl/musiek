//
//  SongPreviewRouter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

protocol SongPreviewRouterProtocol {
    func showErrorAlert(error: String)
}

class SongPreviewRouter: SongPreviewRouterProtocol {
    
    weak var view: SongPreviewViewController?
    
    init(view: SongPreviewViewController) {
        self.view = view
    }
    
    func showErrorAlert(error: String) {
        if let view = view {
            Alerts.dismissableAlert(title: "Error".localized(), message: error, vc: view, actionBtnText: "Cancel".localized())
        }
    }
}
