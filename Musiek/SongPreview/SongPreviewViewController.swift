//
//  SongPreviewViewController.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit
import AVFoundation

protocol SongPreviewViewControllerProtocol: class {
    func setUpUI()
}

class SongPreviewViewController: UIViewController, SongPreviewViewControllerProtocol {
    
    @IBOutlet weak var playButton: UIButton!
    var player : AVPlayer = AVPlayer()
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    lazy var presenter: SongPreviewPresenterProtocol = {
        let router = SongPreviewRouter(view: self) as SongPreviewRouterProtocol
        let api = Networker() as SongPreviewAPIProtocol
        return SongPreviewPresenter(view: self, router: router, api: api)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }

    func setUpUI() {
        playButton.titleLabel?.text = "Play Song Preview"
        if let selectedAlbumTrack = presenter.selectedAlbumTrack {
            self.title = selectedAlbumTrack.trackName
            artistNameLabel.text = selectedAlbumTrack.artistName
            collectionNameLabel.text = selectedAlbumTrack.collectionName
            trackNameLabel.text = selectedAlbumTrack.trackName
            let urlString = selectedAlbumTrack.previewUrl
            guard let url = URL(string: urlString) else {
                print("error to get the song file")
                return
            }
            player = AVPlayer(url: url as URL)
            let localUrlString = selectedAlbumTrack.artworkUrl100
            presenter.downloadImage(urlString: localUrlString) { (data) in
                self.imageView.image = UIImage(data: data)
            }
        }
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        player.play()
    }
}
