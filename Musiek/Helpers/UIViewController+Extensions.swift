//
//  UIViewController+Extensions.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

extension UIViewController{

    func showModalSpinner() -> UIActivityIndicatorView {
        
        // User cannot interact with the app while the spinner is visible
        UIApplication.shared.beginIgnoringInteractionEvents()

        var indicator = UIActivityIndicatorView()

        indicator = UIActivityIndicatorView(frame: self.view.frame)
        indicator.center = self.view.center
        indicator.style = UIActivityIndicatorView.Style.large
        indicator.color = UIColor.black
        indicator.hidesWhenStopped = true
        indicator.startAnimating()

        self.view.addSubview(indicator)

        return indicator
    }

    // Hides the loading indicator and enables user interaction with the app
    func hideModalSpinner(indicator: UIActivityIndicatorView){

        indicator.stopAnimating()
        indicator.isHidden = true

        // user can interact again with the app
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}
