//
//  Alerts.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

class Alerts {
    static func dismissableAlert(title: String,
                                 message: String,
                                 vc: UIViewController,
                                 handler: ((UIAlertAction) -> Void)? = { _ in },
                                 actionBtnText: String,
                                 showCancelButton: Bool = false,
                                 cancelHandler: @escaping (UIAlertAction) -> Void = { _ in }) {

        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        if showCancelButton {
            let dismiss = UIAlertAction(title: "Cancel".localized(),
                                        style: .cancel,
                                        handler: cancelHandler)
            alertController.addAction(dismiss)
        }
        let newAction = UIAlertAction(title: actionBtnText, style: .default, handler: handler)
        alertController.addAction(newAction)

        vc.present(alertController, animated: true, completion: nil)
    }
}
