//
//  MainViewController.swift
//  Musiek
//
//  Created by Diego Vargas on 20-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit
import SwiftUI

protocol SearchViewControllerProtocol: class {
    func setUpUI()
    func nextButtonTapped(_ sender: Any)
}

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SearchViewControllerProtocol{
    
    @IBOutlet weak var enterSearchLabel: UILabel!
    @IBOutlet weak var enterSearchStringField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var presenter: SearchPresenterProtocol = {
        let router = SearchRouter(view: self) as SearchRouterProtocol
        let api = Networker() as SearchAPIProtocol
        return SearchPresenter(view: self, router: router, api: api)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        fetchPreviousSearches()
    }
    
    func setUpUI() {
        self.title = "Search".localized()
        enterSearchLabel.text = "Enter search string".localized()
        nextButton.titleLabel?.text = "Next".localized()
    }
    
    func fetchPreviousSearches() {
        let spinner = self.showModalSpinner()
        presenter.getPreviousSearches(successCompletion: { [weak self](_) in
            self?.hideModalSpinner(indicator: spinner)
            self?.tableView.reloadData()
        }) { [weak self] (error) in
            self?.presenter.showErrorAlert(error: error)
        }
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        guard let searchEntered = enterSearchStringField.text else {return}
        presenter.handleNextButtonTapped(searchEntered: searchEntered)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(presenter.previousSearchesArray.count)
        return presenter.previousSearchesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreviousSearchCell", for: indexPath) as! SearchTableViewCell
        let previousSearch = presenter.previousSearchesArray[indexPath.row]
        cell.previousSearchLabel.text = previousSearch.searchString
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handleDidSelectPreviousSearch(indexPathRow: indexPath.row)
    }
}
