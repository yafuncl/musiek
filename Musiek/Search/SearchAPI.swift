//
//  SearchAPI.swift
//  Musiek
//
//  Created by Diego Vargas on 24-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol SearchAPIProtocol {
    func getPreviousSearches(successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void)
    func saveSearch(searchString: String, successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void)
}

extension Networker: SearchAPIProtocol {
    
    func getPreviousSearches(successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        var previousSearchesArray: [SearchModel] = []
        guard let mainData = UserDefaults.standard.object(forKey: "searchModel") as? NSData
        else {
            failureCompletion("searchModel data not found in UserDefaults")
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: previousSearchesArray, requiringSecureCoding: false)
                UserDefaults.standard.set(data, forKey: "searchModel")
            } catch {
                return
            }
            return
        }
        do {
            guard let finalArray =
            try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(mainData as Data) as? [SearchModel]
            else {
                failureCompletion("Error unarchiving data")
                return
            }
            previousSearchesArray = finalArray
            for search in finalArray {
                print(search.searchString)
            }
            successCompletion(previousSearchesArray)
        }
    }
    
    func saveSearch(searchString: String,successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        var previousSearchesArray: [SearchModel]
        do {
            guard let mainData = UserDefaults.standard.object(forKey: "searchModel") as? NSData else {
                failureCompletion("Error getting searchModel from UserDefaults")
                return
            }
            guard let finalArray =
            try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(mainData as Data) as? [SearchModel]
            else {
                failureCompletion("Error unarchiving data")
                return
            }
            previousSearchesArray = finalArray
        }
        let searchModel = SearchModel(searchString: searchString)
        previousSearchesArray.append(searchModel)
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: previousSearchesArray, requiringSecureCoding: false)
            UserDefaults.standard.set(data, forKey: "searchModel")
            successCompletion(previousSearchesArray)
        } catch {
            failureCompletion(error.localizedDescription)
            return
        }
    }
}
