//
//  SearchModel.swift
//  Musiek
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

class SearchModel: NSObject, NSCoding {
    
    var searchString: String
    
    init(searchString: String) {
        self.searchString = searchString
    }

    required convenience init?(coder decoder: NSCoder) {
        guard let searchString = decoder.decodeObject(forKey: "searchString") as? String
            else {return nil}
        self.init(
            searchString: searchString
        )
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(searchString, forKey: "searchString")
    }
    
}
