//
//  MainRouter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import UIKit

protocol SearchRouterProtocol {
    func showSearchResults(searchEntered: String)
    func showEnterSearchAlert()
    func showErrorAlert(error: String)
}

class SearchRouter: SearchRouterProtocol {
    
    weak var view: SearchViewController?
    
    init(view: SearchViewController) {
        self.view = view
    }
    
    func showSearchResults(searchEntered: String) {
        let storyboard = UIStoryboard(name: "MainStoryboard", bundle: nil)
        let searchResultsViewController = storyboard.instantiateViewController(withIdentifier: "SearchResultsViewController") as! SearchResultsViewController
        searchResultsViewController.presenter.searchEntered = searchEntered
        view?.navigationController?.pushViewController(searchResultsViewController, animated: true)
    }
    
    func showEnterSearchAlert() {
        if let view = view {
            view.navigationController?.popToRootViewController(animated: true)
            Alerts.dismissableAlert(title: "Enter amount".localized(), message: "You need to enter a string".localized(), vc: view, actionBtnText: "Ok".localized())
        }
    }
    
    func showErrorAlert(error: String) {
        if let view = view {
            Alerts.dismissableAlert(title: "Error".localized(), message: error, vc: view, actionBtnText: "Cancel".localized())
        }
    }
}
