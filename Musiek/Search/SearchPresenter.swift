//
//  MainPresenter.swift
//  Musiek
//
//  Created by Diego Vargas on 21-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

protocol SearchPresenterProtocol {
    func getPreviousSearches(successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void)
    func handleNextButtonTapped(searchEntered: String)
    func handleDidSelectPreviousSearch(indexPathRow: Int)
    func showErrorAlert(error: String)
    var previousSearchesArray: [SearchModel] { get set }
    var searchEntered: String? { get set }
}

class SearchPresenter: SearchPresenterProtocol {
    
    var searchEntered: String?
    var previousSearchesArray = [SearchModel]()
    
    weak var view: SearchViewControllerProtocol?
    var router: SearchRouterProtocol
    var api: SearchAPIProtocol
    
    init(view: SearchViewControllerProtocol, router: SearchRouterProtocol, api: SearchAPIProtocol) {
        self.view = view
        self.router = router
        self.api = api
    }
    
    func getPreviousSearches(successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        previousSearchesArray.removeAll()
        api.getPreviousSearches(successCompletion: { [weak self] (receivedPreviousSearches) in
            if let receivedPreviousSearches = receivedPreviousSearches {
                for previousSearch in receivedPreviousSearches {
                    self?.previousSearchesArray.append(previousSearch)
                }
            }
            //self?.previousSearchesArray.reverse()
            successCompletion(self?.previousSearchesArray)
        }) { (error) in
            failureCompletion(error)
        }
    }
    
    func handleNextButtonTapped(searchEntered: String) {
        if !searchEntered.isEmpty {
            let cleanSearch = searchEntered.replacingOccurrences(of: " ", with: "+")
            api.saveSearch(searchString: searchEntered ,successCompletion: { (receivedPreviousSearches) in
                print(receivedPreviousSearches as Any)
            }) { (error) in
                print(error)
            }
            router.showSearchResults(searchEntered: cleanSearch)
        } else {
            router.showEnterSearchAlert()
        }
    }
    
    func handleDidSelectPreviousSearch(indexPathRow: Int) {
        let selectedPreviousSearch = previousSearchesArray[indexPathRow]
        let cleanSearch = selectedPreviousSearch.searchString.replacingOccurrences(of: " ", with: "+")
        router.showSearchResults(searchEntered: cleanSearch)
    }
    
    func showErrorAlert(error: String) {
        router.showErrorAlert(error: error)
    }
}
