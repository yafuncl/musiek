//
//  SearchResultsModelTests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
@testable import Musiek

class SearchResultsModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testEquatableSuccess() {
        let searchResult1 = SearchResultsModel(artistName: "test", trackName: "test", trackId: 1, trackNumber: 1, collectionId: "1", collectionName: "test", artworkUrl100: "test")
        let searchResult2 = SearchResultsModel(artistName: "test", trackName: "test", trackId: 1, trackNumber: 1, collectionId: "1", collectionName: "test", artworkUrl100: "test")
        XCTAssertEqual(searchResult1, searchResult2, "searchResult1 should be equal to searchResult2")
    }
    
    func testEquatableFail() {
        let searchResult1 = SearchResultsModel(artistName: "test", trackName: "test", trackId: 1, trackNumber: 1, collectionId: "1", collectionName: "test", artworkUrl100: "test")
        let searchResult2 = SearchResultsModel(artistName: "test", trackName: "test", trackId: 1, trackNumber: 10, collectionId: "1", collectionName: "test", artworkUrl100: "test")
        XCTAssertNotEqual(searchResult1, searchResult2, "searchResult1 should not be equal to searchResult2")
    }
}
