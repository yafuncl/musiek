//
//  SearchResultsPresenterTests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
@testable import Musiek

class SearchResultsPresenterTests: XCTestCase {
    
    var sut: SearchResultsPresenter!
    
    var mockView: SearchResultsViewControllerMock!
    var mockRouter: SearchResultsRouterMock!
    var mockAPI : SearchResultsAPIMock!

    override func setUp() {
        super.setUp()
        mockView = SearchResultsViewControllerMock()
        mockRouter = SearchResultsRouterMock()
        mockAPI = SearchResultsAPIMock()
        sut = SearchResultsPresenter(view: mockView, router: mockRouter, api: mockAPI)
    }

    override func tearDown() {
        mockView = nil
        mockRouter = nil
        mockAPI = nil
        sut = nil
        super.tearDown()
    }
    
    func testGetSearchResults() {
        sut.getResults(successCompletion: { (_) in }) { (_) in }
        XCTAssertTrue(mockAPI.getSearchResultsCalled)
    }
    
    func testDownloadImage() {
        sut.downloadImage(urlString: "test") { (_) in
        }
        XCTAssertTrue(mockAPI.downloadImageCalled)
    }
    
    func testShowErrorAlert() {
        sut.showErrorAlert(error: "test")
        XCTAssertTrue(mockRouter.showErrorAlertCalled)
    }

}
