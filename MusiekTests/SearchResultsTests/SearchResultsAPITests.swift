//
//  SearchResultsAPITests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
import OHHTTPStubs

@testable import Musiek

class SearchResultsAPITests: XCTestCase {
    
    var sut: SearchResultsAPIProtocol!

    override func setUp() {
        super.setUp()
        sut = Networker() as SearchResultsAPIProtocol
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testGetSearchResultsSuccess() {
        stub(condition: isHost("itunes.apple.com")) { _ in
            let stubPath = OHPathForFile("searchResultsData.json", type(of:self))
            return HTTPStubsResponse(fileAtPath: stubPath!, statusCode: 200, headers: ["Content-Type":"application/json"])
        }
        let expectation = self.expectation(description: "calls the callback with a resource object")
        sut.getSearchResults(searchString: "in+utero", successCompletion: { (receivedSearchResults) in
            XCTAssertEqual(receivedSearchResults?.first?.trackName, "In Utero")
            XCTAssertEqual(receivedSearchResults?.first?.artistName, "Californication")
            XCTAssertEqual(receivedSearchResults?.first?.trackId, 309564409)
            XCTAssertEqual(receivedSearchResults?.first?.collectionId, "309366086")
            expectation.fulfill()
        }) { (error) in
        }
        self.waitForExpectations(timeout: 0.3, handler: nil)
    }
    
    func testGetSearchResultsWrongData() {
        stub(condition: isHost("itunes.apple.com")) { _ in
            let stubPath = OHPathForFile("searchResultsData.json", type(of:self))
            return HTTPStubsResponse(fileAtPath: stubPath!, statusCode: 200, headers: ["Content-Type":"application/json"])
        }
        let expectation = self.expectation(description: "calls the callback with a resource object")
        sut.getSearchResults(searchString: "in+utero", successCompletion: { (receivedSearchResults) in
            XCTAssertNotEqual(receivedSearchResults?.first?.trackName, "wrong")
            XCTAssertNotEqual(receivedSearchResults?.first?.artistName, "wrong")
            XCTAssertNotEqual(receivedSearchResults?.first?.trackId, 309564402)
            XCTAssertNotEqual(receivedSearchResults?.first?.collectionId, "wrong")
            expectation.fulfill()
        }) { (error) in
        }
        self.waitForExpectations(timeout: 0.3, handler: nil)
    }
    
    func testGetSearchResultsFail() {
        stub(condition: isHost("itunes.apple.com")) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }
        let expectation = self.expectation(description: "down network")
        
        sut.getSearchResults(searchString: "in+utero", successCompletion: { (receivedSearchResults) in
        }) { (error) in
            print("here is the error: ", error)
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 0.3, handler: nil)
    }

}
