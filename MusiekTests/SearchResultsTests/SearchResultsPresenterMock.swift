//
//  SearchResultsPresenterMock.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
@testable import Musiek

class SearchResultsPresenterMock: SearchResultsPresenterProtocol {
    
    var searchResultsArray: [SearchResultsModel] = [SearchResultsModel(artistName: "test", trackName: "test", trackId: 1, trackNumber: 1, collectionId: "1", collectionName: "test", artworkUrl100: "test"),SearchResultsModel(artistName: "test", trackName: "test", trackId: 2, trackNumber: 2, collectionId: "1", collectionName: "test", artworkUrl100: "test")]
    var searchEntered: String?
    
    var getResultsCalled = false
    func getResults(successCompletion: @escaping ([SearchResultsModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        getResultsCalled = true
        showErrorAlert(error: "test")
    }
    var downloadImageCalled = false
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void) {
        downloadImageCalled = true
    }
    var showErrorAlertCalled = false
    func showErrorAlert(error: String) {
        showErrorAlertCalled = true
    }
    var handleDidSelectRowCalled = false
    func handleDidSelectRow(indexPathRow: Int) {
        handleDidSelectRowCalled = true
    }
    
    
    
    
}
