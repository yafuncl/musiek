//
//  SearchResultsViewControllerTests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
@testable import Musiek

class SearchResultsViewControllerTests: XCTestCase {
    
    var window: UIWindow!
    var sut: SearchResultsViewController!
    var mockPresenter: SearchResultsPresenterMock!

    override func setUp() {
        super.setUp()
        window = UIWindow()
        mockPresenter = SearchResultsPresenterMock()
        sut = UIStoryboard(name: "MainStoryboard", bundle: nil).instantiateViewController(identifier: "SearchResultsViewController") as? SearchResultsViewController
        sut.presenter = self.mockPresenter
        window.rootViewController = sut
        window.makeKeyAndVisible()
        _ = sut.view
    }

    override func tearDown() {
        mockPresenter = nil
        sut = nil
        self.window = nil
        super.tearDown()
    }
    
    func testSetUpUI() {
        XCTAssertEqual(sut.title, nil)
        XCTAssertTrue(mockPresenter.getResultsCalled)
        XCTAssertTrue(mockPresenter.showErrorAlertCalled)
        XCTAssertFalse(mockPresenter.downloadImageCalled)
    }
    
    func testViewDidLoad() {
        XCTAssertEqual(sut.title, nil)
        XCTAssertTrue(mockPresenter.getResultsCalled)
        XCTAssertTrue(mockPresenter.showErrorAlertCalled)
        XCTAssertFalse(mockPresenter.downloadImageCalled)
    }
    
    func testViewSearchResultsModelArrayNotEmpty() {
        XCTAssertEqual(sut.tableView.numberOfRows(inSection: 0), 2)
    }
    
    func testViewSearchResultsModelArrayEmpty() {
        mockPresenter.searchResultsArray.removeAll()
        sut.tableView.reloadData()
        XCTAssertEqual(sut.tableView.numberOfRows(inSection: 0), 0)
    }
    
    func testDidSelectRowAt() {
        sut.tableView(sut.tableView, didSelectRowAt: IndexPath(item:0, section: 0))
        XCTAssertTrue(mockPresenter.handleDidSelectRowCalled)
    }

}
