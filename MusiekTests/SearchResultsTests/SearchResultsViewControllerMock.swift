
//
//  SearchResultsViewControllerMock.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
@testable import Musiek

class SearchResultsViewControllerMock: SearchResultsViewControllerProtocol {
    var setUpUICalled = false
    func setUpUI() {
        setUpUICalled = true
    }
}
