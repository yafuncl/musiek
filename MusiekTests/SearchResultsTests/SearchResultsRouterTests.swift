//
//  SearchResultsRouterTests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
@testable import Musiek

class SearchResultsRouterTests: XCTestCase {
    
    var sut: SearchResultsRouter!
    var viewController: SearchResultsViewController!
    var window: UIWindow!

    override func setUp() {
        super.setUp()
        window = UIWindow()
        viewController = UIStoryboard(name: "MainStoryboard", bundle: nil).instantiateViewController(identifier: "SearchResultsViewController") as? SearchResultsViewController
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        _ = viewController.view
        sut = SearchResultsRouter(view: viewController)
    }

    override func tearDown() {
        viewController = nil
        sut = nil
        window = nil
        super.tearDown()
    }
    
    func testShowErrorAlert() {
        sut.showErrorAlert(error: "test")
        XCTAssertTrue(viewController.presentedViewController is UIAlertController)
    }

}
