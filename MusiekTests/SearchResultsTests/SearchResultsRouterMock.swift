//
//  SearchResultsRouterMock.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
@testable import Musiek

class SearchResultsRouterMock: SearchResultsRouterProtocol {
    var showResultSelectCalled = false
    func showResultSelect(searchEntered: String, selectedSearchResult: SearchResultsModel) {
        showResultSelectCalled = true
    }
    var showErrorAlertCalled = false
    func showErrorAlert(error: String) {
        showErrorAlertCalled = true
    }
}
