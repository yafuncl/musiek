//
//  SearchResultsAPIMock.swift
//  MusiekTests
//
//  Created by Diego Vargas on 23-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
@testable import Musiek

class SearchResultsAPIMock: SearchResultsAPIProtocol {
    var getSearchResultsCalled = false
    func getSearchResults(searchString: String?, successCompletion: @escaping ([SearchResultsModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        getSearchResultsCalled = true
    }
    var downloadImageCalled = false
    func downloadImage(urlString: String, completion: @escaping (Data) -> Void) {
        downloadImageCalled = true
    }
}
