//
//  SearchPresenterMocks.swift
//  MusiekTests
//
//  Created by Diego Vargas on 22-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
@testable import Musiek

class SearchPresenterMock: SearchPresenterProtocol {
    var getPreviousSearchesCalled = false
    func getPreviousSearches(successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        getPreviousSearchesCalled = true
    }
    
    var handleDidSelectPreviousSearchCalled = false
    func handleDidSelectPreviousSearch(indexPathRow: Int) {
        handleDidSelectPreviousSearchCalled = true
    }
    var showErrorAlertCalled = false
    func showErrorAlert(error: String) {
        showErrorAlertCalled = true
    }
    
    var previousSearchesArray: [SearchModel] = []
    
    var searchEntered: String?
    
    var handleNextButtonTappedCalled = false
    func handleNextButtonTapped(searchEntered: String) {
        handleNextButtonTappedCalled = true
    }
}
