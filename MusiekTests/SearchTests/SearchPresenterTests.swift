//
//  SearchPresenterTests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 22-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
@testable import Musiek

class SearchPresenterTests: XCTestCase {
    
    var sut: SearchPresenter!
    
    var mockView: SearchViewControllerMock!
    var mockRouter: SearchRouterMock!
    var mockAPI: SearchAPIMock!
    
    override func setUp() {
        super.setUp()
        mockView = SearchViewControllerMock()
        mockRouter = SearchRouterMock()
        mockAPI = SearchAPIMock()
        sut = SearchPresenter(view: mockView, router: mockRouter, api: mockAPI)
    }
    
    override func tearDown() {
        mockView = nil
        mockRouter = nil
        sut = nil
        super.tearDown()
    }
    
    func testHandleNextButtonTapped() {
        sut.handleNextButtonTapped(searchEntered: "in utero")
        XCTAssertTrue(mockRouter.showSearchResultsCalled, "Mock router show search results show have been called")
    }
}
