//
//  SearchRouterTests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 22-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
@testable import Musiek

class SearchRouterTests: XCTestCase {
    
    var sut: SearchRouter!
    var viewController: SearchViewController!
    var window: UIWindow!

    override func setUp() {
        super.setUp()
        window = UIWindow()
        viewController = UIStoryboard(name: "MainStoryboard", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        _ = viewController.view
        sut = SearchRouter(view: viewController)
    }

    override func tearDown() {
        viewController = nil
        sut = nil
        window = nil
        super.tearDown()
    }
    
    func testShowSearchResults() {
        sut.showSearchResults(searchEntered: "in utero")
        XCTAssertNotNil(SearchResultsViewController.self, "Should show SearchResultsViewController")
    }
    
}
