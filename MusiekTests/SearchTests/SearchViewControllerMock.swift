//
//  SearchViewControllerMock.swift
//  MusiekTests
//
//  Created by Diego Vargas on 22-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
@testable import Musiek

class SearchViewControllerMock: SearchViewControllerProtocol {
    
    var setUpUiCalled = false
    func setUpUI() {
        setUpUiCalled = true
    }
    
    var nextButtonTappedCalled = false
    func nextButtonTapped(_ sender: Any) {
        nextButtonTappedCalled = true
    }
}

