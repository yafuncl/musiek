//
//  SearchAPIMock.swift
//  MusiekTests
//
//  Created by Diego Vargas on 24-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation

@testable import Musiek

class SearchAPIMock: SearchAPIProtocol {
    
    var getPreviousSearchesCalled = false
    func getPreviousSearches(successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        getPreviousSearchesCalled = true
    }
    
    var saveSearchCalled = false
    func saveSearch(searchString: String, successCompletion: @escaping ([SearchModel]?) -> Void, failureCompletion: @escaping (String) -> Void) {
        saveSearchCalled = true
    }
    
    
}

