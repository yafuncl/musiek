//
//  SearchViewControllerTests.swift
//  MusiekTests
//
//  Created by Diego Vargas on 22-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import XCTest
@testable import Musiek

class SearchViewControllerTests: XCTestCase {
    
    var window: UIWindow!
    var sut: SearchViewController!
    var mockPresenter: SearchPresenterMock!
    var mockAPI: SearchAPIMock!

    override func setUp() {
        super.setUp()
        window = UIWindow()
        sut = UIStoryboard(name: "MainStoryboard", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
        mockPresenter = SearchPresenterMock()
        sut.presenter = self.mockPresenter
        window.rootViewController = sut
        window.makeKeyAndVisible()
        _ = sut.view
    }

    override func tearDown() {
        mockPresenter = nil
        sut = nil
        self.window = nil
        super.tearDown()
    }
    
    func testSetUpUI() {
        sut.setUpUI()
        XCTAssertEqual(sut.title, "Search".localized(), "Navigation title should be 'Next' localized")
        XCTAssertEqual(sut.enterSearchLabel.text, "Enter search string".localized(), "Enter search label should be 'Enter search string' localized")
        XCTAssertEqual(sut.nextButton.titleLabel?.text, "Search".localized(), "Button label should be 'next' localized")
    }
    
    func testNextButtonTapped() {
        sut.enterSearchStringField.text = "in utero"
        sut.nextButtonTapped(UIButton())
        XCTAssertTrue(mockPresenter.handleNextButtonTappedCalled    )
    }
}
