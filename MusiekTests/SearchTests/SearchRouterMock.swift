//
//  SearchRouterMock.swift
//  MusiekTests
//
//  Created by Diego Vargas on 22-03-20.
//  Copyright © 2020 Diego Vargas. All rights reserved.
//

import Foundation
@testable import Musiek

class SearchRouterMock: SearchRouterProtocol {
    
    var showErrorAlertCalled = false
    func showErrorAlert(error: String) {
        showErrorAlertCalled = true
    }
    
    
    var showSearchResultsCalled = false
    func showSearchResults(searchEntered: String) {
        showSearchResultsCalled = true
    }
    
    var showEnterSearchAlertCalled = false
    func showEnterSearchAlert() {
        showEnterSearchAlertCalled = true
    }
}
